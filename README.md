# lemmi
Code for building the containers included on https://lemmi.ezlab.org/

To get help: https://gitlab.com/ezlab/lemmi/issues

The kaiju container has a demo Snakemake pipeline and demo input files

after `cd /path/to/container && docker build . -t container_name` (or `docker load` if you downloaded an archive) + creating the [bbx](https://www.ezlab.org/lemmi_userguide.html#host-container-exchange-files-and-folders-provided-inputs-expected-outputs) folder hierarchy on your machine

- run the *build_ref* task: `docker run -u your_user_uid -v "/path/to/container/bbx:/bbx:rw" -e "taxlevel=species" -e "reference=reference_name" -e "thread=1" --cpus 1 -m "4G" container_name build_ref`

- run the *analysis* task: `docker run -u your_user_uid -v "/path/to/container/bbx:/bbx:rw" -e "taxlevel=species" -e "reference=reference_name" -e "thread=1" --cpus 1 -m "4G" container_name analysis`

or run the container interactively: `docker run -it --entrypoint=/bin/bash -v /path/to/container/bbx:/bbx/ -u your_user_uid container_name`


Requirements:

- Docker 18.09.0 or later. Containers were tested on Linux environments. It should properly work on installed MacOS and Windows docker environment (https://www.youtube.com/watch?v=S7NVloq0EBc)
- Most containers should be successfully built on standard hardware. Ganon may require edit of the parameters on older server (see Ganon Dockerfile)
- Runtime and memory depends on tools and inputs

**Disclaimer**: `docker run` uses the root account by default. We discourage this practice and the containers we provide are designed to be run using a non-root uid (-u uid). We decline responsibility for any damage that could result from using these container sources.  