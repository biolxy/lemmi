#!/bin/bash

cut -f 1,2 /bbx/tmp/out.txt.lca > /bbx/tmp/out_clean

echo "# Taxonomic Binning Output
@SampleID:ganon
@Version:0.9.1
@Ranks:superkingdom|phylum|class|order|family|genus|species
@@SEQUENCEID	BINID" | cat - /bbx/tmp/out_clean > /bbx/tmp/bins.txt
rm /bbx/tmp/out_clean

# make empty profile file

touch /bbx/tmp/profile.txt



