import re
import os
import gzip
import glob

for entry in glob.glob('/bbx/input/training/*.fna.gz'):
    outp = open('/bbx/tmp/%s' % entry.split('/')[-1].split('.gz')[0], 'w')
    with gzip.open(entry, 'rb') as f:
        for line in f:
            outp.write(line.decode('utf-8'))
    outp.close()

outp = open('/bbx/tmp/all.fna', 'w')


with open('/bbx/input/training/mapping.tsv', 'r') as tsvin:
    for row in tsvin:
        if not (row.startswith('#') or row.startswith('@')):
            taxid = row.strip().split('\t')[0]
            accession = row.strip().split('\t')[4]
            for line in open('/bbx/tmp/%s.fna' % accession):
                if line.startswith(">"):
                    line = line.replace(","," ")
                    splited_rec = re.split(r'(>| )', line)
                    accession = splited_rec[2]
                    line = ">" + taxid + "|" + "".join(splited_rec[2:])
                    outp.write(line)
                else:
                    outp.write(line)
outp.close()
