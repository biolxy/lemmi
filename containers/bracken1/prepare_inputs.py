import os
for line in open('/bbx/input/training/mapping.tsv'):
    if line.startswith('#') or line.startswith('@'):
        continue
    name = line.strip().split('\t')[4]
    taxid = line.strip().split('\t')[0]
    inp = '/bbx/tmp/%s.fa' % name
    # edit the header as follow >id|kraken:taxid|xxxxx
    outp = open('/bbx/tmp/input/%s.fa' % name, 'w')
    for line2 in open(inp, 'r'):
        if line2.startswith('>'):
            line2 = '%s|kraken:taxid|%s\n' % (line2.strip().split(' ')[0], taxid)
        outp.write(line2)
    outp.close()
    os.remove(inp) # save disk space
