LEMMI container for Kaiju with a simple Snakemake pipeline to run it. It builds a reference from protein fasta and runs an analysis of paired-end fastq.

- You need to replace the LEMMI mapping in bbx/input/training/mapping.tsv with your own list following that format.
- You need to provide a faa.gz file in bbx/input/training/ for each entry in the mapping
- You need to place your paired end reads in bbx/input/testing/reads.1.fq.gz bbx/input/testing/reads.2.fq.gz
- You need to place NCBI dmp files in bbx/input/training/ (current file are empty and need to be replaced)
    - from here: ftp://ftp.ncbi.nlm.nih.gov/pub/taxonomy/taxdump.tar.gz

Then `snakemake -r --config uid=your_user_uid`

To run the container directly: `docker run -it --entrypoint=/bin/bash -v /path/to/host/bbx:/bbx/ -u your_user_uid kaiju`

- If you have several samples, replace the input reads, move/delete the existing outputs and rerun, Snakemake will reuse the already built database

---


If you wish to use the Kaiju container as a basis for creating a new LEMMI container with your own method, you will need to:  
- Edit the `Dockerfile` so it installs your software. If you use git, please checkout a tag or a specific commit.
- Edit the `Taskfile` to enter the commands required to build a reference and process to an analysis that output (i) a profile and (ii) a binning report. If not possible to produce both, write an empty file.
- Add any scripts needed in the `scripts` folder and in the `Dockerfile`
- Don't directly use bash commands with a limited list of arguments (i.e. ls, rm, etc.) when processing the reference genomes found in mapping.tsv, as in an actual LEMMI benchmark it will likely exceed this limit. Use `find -exec`, see `scripts/prepare_inputs.sh` for an example.
- Be aware the analysis has to run with a non-root user (test the container with a uid that is not).
- Providing the Snakefile is optional, it is a way for you to test the container. The goal is that it works with the content of the `bbx` folder and produces the expected outputs.
- Nucleotide inputs can be found in https://gitlab.com/ezlab/lemmi/tree/master/containers/demo_kraken/bbx/input/training/ to replace the protein content of the `bbx/input/training` used by kaiju.
- No significant information can be extracted from the fasta header of the input genomes. Only the mapping prevail, up to you to write accessory scripts to reprocess the inputs and outputs.
- If the fake reads included with the container (i.e. a single sequence in `/bbx/input/testing/`) are not enough for your tests, a real example of a dataset that could be run on the LEMMI platform is available on https://zenodo.org/record/2651062. Up to you to define reference genomes and a mapping for your tests in `/bbx/input/training/`.
- Then contact us to validate the container, or for help, and to plan the benchmark your method. We are looking forward to have it in our list.